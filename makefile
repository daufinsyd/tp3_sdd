all: main

dbg: main_dbg

main_dbg: file.o pile.o main.o
	gcc -o run -g file.o pile.o main.o -Wall -Wextra -ansi -pedantic

main: file.o pile.o main.o
	gcc -o run file.o pile.o main.o -W -Wall -ansi -pedantic

### Objects ###
file.o: file.c
	gcc -o file.o -c file.c -W -Wall -ansi -pedantic

pile.o: pile.c
	gcc -o pile.o -c pile.c -W -Wall -ansi -pedantic

main.o: main.c
	gcc -o main.o -c main.c -W -Wall -ansi -pedantic

### Post build ###
clean:
	rm *.o

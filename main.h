/*
// Created by sygems on 4/28/16.
*/

#ifndef TP_SDD_MAI_MAIN_H
#define TP_SDD_MAI_MAIN_H


typedef struct tree{

    char name;
    struct tree *son;
    struct tree *brother;

} tree;


#endif /*TP_SDD_MAI_MAIN_H*/

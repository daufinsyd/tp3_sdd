#ifndef PILE_H
#define PILE_H

#include <stdio.h>
#include <stdlib.h>
#include "main.h"

typedef tree* typepile; /* type des ??l??ments contenus dans la pile. Si on veux manipuler des char, on a qu'?? changer le int en char et modifier les %d pr??sent dans les diff??rentes fonctions qui interagissent avec l'utilisateur */

typedef struct pile {
	int TailleMax;
	int Sommet;
	typepile* Base;
}pile_t;

pile_t* InitialisationPile(int taille);

pile_t* InitialisationPile2();

int Empiler(pile_t * pile, typepile val);

typepile Depiler(pile_t* pile);

typepile Sommet(pile_t* pile);

int EstVidePile(pile_t* pile);

int EstVidePile2(pile_t* pile);

void LibererPile(pile_t* pile);

#endif

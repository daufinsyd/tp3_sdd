#include "pile.h"

pile_t* InitialisationPile(int taille)
{
	pile_t* pile=NULL;
	typepile* base=NULL;
	
	pile=(pile_t*)malloc(sizeof(pile_t));
	base=(typepile*)malloc(taille*sizeof(typepile));
	
	if ( (pile!=NULL)&&(base!=NULL) )
	{
		pile->TailleMax=taille;
		pile->Sommet=-1;
		pile->Base=base;
	}
	return pile;
}

pile_t* InitialisationPile2()
{
	int taille;
	
	printf("De quelle taille est la pile que vous voulez initialiser ?\n");
	scanf("%d",&taille);
	
	return InitialisationPile(taille);
}




int Empiler(pile_t * pile, typepile val)
{
	int rep=0;
	
	if (pile->TailleMax > pile->Sommet+1)
	{
		pile->Sommet+=1;
		pile->Base[pile->Sommet+1]=val;
		rep=1; 
	}
	
	return rep;
}





typepile Depiler(pile_t* pile)
{
	typepile val;
	
	val=pile->Base[pile->Sommet +1];
	pile->Sommet--;
	
	return val;
}





typepile Sommet(pile_t* pile)
{
	return pile->Base[pile->Sommet +1];
}




int EstVidePile(pile_t* pile)
{
	int rep=0;
	
	if(pile->Sommet==-1)
	{
		rep=1;
	}
	return rep;
}

int EstVidePile2(pile_t* pile)
{
	int rep=EstVidePile(pile);
	
	if(rep==1)
	{
		printf("La pile est vide.\n");
	}
	else
	{
		printf("La pile n'est pas vide.\n");
	}
	return rep;
}




void LibererPile(pile_t* pile)
{
	if(pile!=NULL)
	{
		free(pile->Base);
		free(pile);
		pile=NULL;
	}
}



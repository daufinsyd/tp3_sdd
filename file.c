#include "file.h"

file_t * InitialisationFile(int taille)
{
	file_t* file=NULL;
	typefile* base=NULL;
		
	file=(file_t*)malloc(sizeof(file_t));
	base=(typefile*)malloc(taille*sizeof(typefile));
	
	if (base!=NULL && file!=NULL)
	{
		file->TailleMemoire=taille; /*Nb de case qu'on a*/
		file->NbEl=0;
		file->Base=base;
		file->debut=0;
		file->fin=taille-1;
	}
	return file;	
}

file_t* InitialisationFile2()
{
	int taille;
	file_t* file=NULL;
	
	printf("De quelle taille est la file que vous voulez initialiser ?\n");
	scanf("%d",&taille);
	
	file=InitialisationFile(taille);
	
	if(file==NULL)
	{
		printf("Problème\n");
	}
	
	return file;
}





int EntreeFile(file_t* file, typefile val)
{
	int rep=1; 
	
	if (file->TailleMemoire!=file->NbEl) /*file pas pleine*/
	{
		if (file->fin==file->TailleMemoire-1)
		{
			file->fin=0;
			file->Base[0]=val;
			file->NbEl++;
			rep=0;
		}
		else
		{
			file->fin++;
			file->Base[file->fin]=val;
			file->NbEl++;
			rep=0;
		}
	}
	return rep;
}


typefile SortieFile(file_t* file)
{
	typefile val;
	
	if (file->debut==file->fin)
	{
		val=file->Base[file->debut];
		file->debut=0;
		file->fin=file->TailleMemoire-1;
		file->NbEl=0;
	}
	else
	{
		val=file->Base[file->debut];
		file->NbEl--;
		
		if(file->debut==file->TailleMemoire-1)
		{
			file->debut=0;
		}
		else
		{
			file->debut++;
		}
	}
	return val;
}





int EstVideFile(file_t* file)
{
	int rep=0;

	/* Si la file n'est pas vide, rep = 0 */
	if(file->NbEl == 0)
	{
		rep=1;
	}
	return rep;
}

int EstVideFile2(file_t* file)
{
	/* NON */
	int rep=EstVideFile(file);
	if(rep==1)
	{
		printf("La file est vide.\n");
	}
	else
	{
		printf("La file n'est pas vide.\n");
	}
	return rep;
}

void LibererFile(file_t* file)
 {
 	if (file!=NULL)
 	{
 		free(file->Base);
 		free(file);
 		file=NULL;
 	}
 }



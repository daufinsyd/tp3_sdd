#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "pile.h"
#include "file.h"

#define TAILLE_MAX 50


tree* createBranch(int nb) {
    /* cr?e la branche avec tous les fr?res */

    int i;
    tree *currentCase=NULL, *firstCase=NULL, *currentBrotherSav=NULL;

    firstCase = malloc(sizeof(tree));


    if (firstCase) {
        firstCase->name = 'e';
        currentCase = firstCase;
        for (i = 0; i < nb - 1; i++) {
            currentCase->brother = malloc(sizeof(tree));
            currentCase = currentCase->brother;
            if (!currentCase) {
                currentCase = firstCase;
                for (i; i>0; i--) {
                    currentBrotherSav = currentCase;
                    currentCase = currentCase->brother;
                    free(currentBrotherSav);
                }
            }
        }
    }
    else {
        free(firstCase);
    }
    if (currentCase) {
        currentCase->brother = NULL;
    }
    fflush(stdout);
    return firstCase;
}

tree* createTree(char* fileName) {
    /*nbNode: premier caractere lu */

    tree *head = NULL, *i = NULL;
    int nbNode;
    char currentChar;

    /* Initialisation de la pile */
    pile_t* pile = InitialisationPile(TAILLE_MAX);

    FILE* file = fopen(fileName, "r");
    if (file) {

        head =  malloc(sizeof(tree *));

        if (head) {

            /* Initialisation */
            fscanf(file, "%d", &nbNode);
            fflush(stdout);
            head = createBranch(nbNode);
            i = head;

            while (i != NULL) {

                fscanf(file, "%c", &currentChar);
                i->name = currentChar;
                printf("%c %c\n", head->name, i->name);
                fflush(stdout);

                fscanf(file, "%d", &nbNode);
                if (!nbNode) {
                    i->son = NULL;
                    while (i->brother == NULL && !EstVidePile(pile)) {
                        i = Depiler(pile);
                    }
                    i = i->brother;

                }
                else {
                    Empiler(pile, i);
                    fflush(stdout);
                    i->son = createBranch(nbNode);
                    i = i->son;
                }
            }
        }
        else {
            /* probleme d'allocation de m?morie */
            free(head);
        }
    }

    fclose(file);

    LibererPile(pile);

    return head;
}


tree* searchFather(tree* root, char value) {
    /* Search node by value */
    tree* i=root;
    file_t* file = InitialisationFile(TAILLE_MAX);


    if ( !(i->brother == NULL && i->son == NULL) ) {

        /* a la fin, si le pere n'existe pas, on est sur le premier ?l?ment et la file est vide */
        /* si il y a un seul ?l?ment, alors i est toujours ?gal ? racine et donc le while ne fini jamais */
        while ( i->name != value && !(EstVideFile(file) && i->brother == NULL && i->son == NULL) ) {
            printf("::%c", i->name);
            if ( i->son != NULL ) {
                /* il est inutile d'enfiler si le noeud n'a pas de fils, car le principe est de pouvoir
                 * acceder au niveau inferieur par l'element stocke dans la file */
                EntreeFile(file, i);
            }
            if ( i->brother != NULL ) {

                i = i->brother;
            }
            else {
                if (  !EstVideFile(file) ) {
                    /* On revient au premier element de la pile
                     * et on va au niveau inf?rieur (son fils) */
                    i = SortieFile(file);
                }
                i = i->son;

                }
            }
        }

    LibererFile(file);

    return i;
}

/* Une fois le p?re trouve, il faut trouver la position parmis ses fils ou inserer son nouveau fils.
 * On parcours la liste de ses fils tant que le fils visite a une valeur superieur au fils que l'on
 * souhaite inserer */
tree* searchBrotherPosition(tree* father, char sonValue) {
    tree* i=father;

    if ( father->son && father->son->name < sonValue) {
        i = father->son;
        while ( i->name < sonValue && i->brother ) {
            i = i->brother;
        }
    }

    return i;
}

/* Fonction d'insertion d'un fils.
 * La fonction insere un fils au pere souhaite dans l'ordre croissant des noms.
 * La fonction renvoie un code d'erreur pour signaler si l'insertion s'est bien passee ou non */
int addSon(tree* root, char fatherName, char sonName) {
    tree *father=NULL, *son=NULL, *newSon=NULL;
    int code = 1; /* code==1 >> error */

    /* On cherche le pere */
    father = searchFather(root, fatherName);
    if (father) {
        /* Insertion du fils de maniere ordon?e */
        son = searchBrotherPosition(father, sonName);

        newSon = malloc(sizeof(tree));
        if ( newSon ) {
            if (son == father) {
                /* Le pere n'a pas encore de fils ou il faut l'ajouter en premier fils */

                newSon->brother = father->son;
                father->son = newSon;

            }
            else {
                newSon = malloc(sizeof(tree));
                newSon->brother = son->brother;
                son->brother = newSon;
            }
            newSon->name = sonName;
            newSon->son = NULL;
            code = 0;
        }

    }
    return code;
}


int compt_fils (tree* i)
{
    int compteur=0;
    tree* e=i->son;
    while(e!=NULL)
    {
        compteur+=1;
        e=e->brother;
    }
    return compteur;
}

void representationPostfixee(tree* arbre)
{
    tree* i=arbre;
    pile_t* pile = InitialisationPile(TAILLE_MAX);

    while (i!=NULL)
    {
        while(i->son!=NULL)
        {
            Empiler(pile,i);
            i=i->son;
        }
        printf("%c %d ", i->name, compt_fils(i));

        while(i->brother==NULL && EstVidePile(pile)!=1)
        {
            i=Depiler(pile);
            printf("%c %d ", i->name, compt_fils(i));
        }
        i=i->brother;
    }

    LibererPile(pile);
}

int freeTree(tree* root) {
    tree* i = root, *prec = NULL;

    file_t* file = InitialisationFile(TAILLE_MAX);
    while ( !(EstVideFile(file) && i->son == NULL && i->brother == NULL) ) {
        printf("%c ", i->name);
        if ( i->son != NULL ) {
            /* On sauvegarde l'adresse du fils */
            EntreeFile( file, i->son );
        }
        /* On avance au cadet puis on libere l'aine */
        prec = i;
        if ( i->brother != NULL ) {
            /* si il a un frere, on continue */
            i = i->brother;
        }
        else {
            i = SortieFile(file);
        }
        free(prec);
    }
    free(root);
    free(i);

    LibererFile(file);

    return 0;

}

int main(int agrc, char* agrv[]) {

    tree *newTree = NULL;
    int code;

    printf("%s\n", agrv[1]);
    fflush(stdout);

    newTree = createTree(agrv[1]);

    fflush(stdout);
    representationPostfixee(newTree);
    code = addSon(newTree, 'C', 'A');
    representationPostfixee(newTree);
    code = freeTree(newTree);


    printf("\n\n Lib?: %p", newTree);
    /*representationPostfixee(newTree);*/

    return 0;
}
#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdlib.h>
#include "main.h"

typedef tree* typefile;

typedef struct file{
	int TailleMemoire;
	int NbEl;
	int debut; /*indice de la première valeur dans le tableau de Base*/
	int fin; /*indice de la dernière valeur dans le tableau de Base*/
	typefile* Base;
}file_t;

file_t * InitialisationFile(int taille);

file_t* InitialisationFile2();

int EntreeFile(file_t* file, typefile val);

typefile SortieFile(file_t* file);

int EstVideFile(file_t* file);

int EstVideFile2(file_t* file);

void LibererFile(file_t* file);

#endif
